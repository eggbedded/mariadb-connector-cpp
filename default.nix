{ pkgs, cmake, fetchgit }:

let
  generic = { version, sha256 }:
    pkgs.stdenv.mkDerivation {
        pname = "mariadb-connector-cpp";
        inherit version;

        src = fetchgit {
          url = "https://github.com/MariaDB-Corporation/mariadb-connector-cpp.git";
          rev = "${version}";
          inherit sha256;
        };

        nativeBuildInputs = [ cmake ];
        buildInputs = [ pkgs.openssl ];

        cmakeFlags = [
          "-DCONC_WITH_UNIT_TESTS=Off"
          "-DWITH_SSL=OPENSSL"
        ];

        postFixup = ''
          ln -s $out/lib/mariadb/libmariadb.so.3 $out/lib/libmariadb.so.3
          ln -s $out/lib/mariadb/libmariadbcpp.so $out/lib/libmariadbcpp.so
        '';

        };
in
{
  mariadb-connector-cpp_1_0_1 = generic {
    version = "1.0.1";
    sha256 = "0L7jUKBdxIhtl1TsKQBESmxPEbgZ/1j1NGjAqxZzkmA=";
  };
}