{
  description = "A flake for C++ mariadb connector";

  inputs = {

    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        pkgsArmCross = import nixpkgs { inherit system; crossSystem = { config = "aarch64-unknown-linux-gnu";}; };
        code = pkgs.callPackage ./. { };
        codeArmCross = pkgsArmCross.callPackage ./. { };
      in rec {
        packages.default = code.mariadb-connector-cpp_1_0_1;
        packages.armCross = codeArmCross.mariadb-connector-cpp_1_0_1;
      }
      );
}
